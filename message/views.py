# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from .models import UserMessage


# Create your views here.

# 获取表单类
def getform(request):
    message = None
    name = request.GET.get('name', '')
    all_messages = UserMessage.objects.filter(name=name)
    if all_messages:
        message = all_messages[0]

    if request.method == 'POST':
        user_message = UserMessage()
        user_message.name = request.POST.get('name', '')
        user_message.email = request.POST.get('email', '')
        user_message.address = request.POST.get('address', '')
        user_message.message = request.POST.get('message', '')
        user_message.uid = request.POST.get('uid', '')
        user_message.save()
    # user_message = UserMessage()
    # user_message.name = "zhoujing"
    # user_message.address = "北京"
    # user_message.message = "再见"
    # user_message.email = "12232@qq.com"
    # user_message.uid = "444"
    # user_message.save()
    return render(request, 'message_form.html', {
        "my_message": message
    })
